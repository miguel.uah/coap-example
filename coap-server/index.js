const coap = require('coap') 

const port = 5683

const coap_server = coap.createServer( (req, res) => {
    
    if (req.headers['Content-Type'] != 'application/json') {
        res.code = '4.06'
        return res.end()
    }
    
    res.setOption('Accept', 'application/json')
    
})
 
coap_server.on('request', (req, res) => {
    console.log('---------------')
    console.log('New request for ' + req.url + '\n')
  
    if(req.url === '/date' && req.method === 'GET'){
        res.end(
            JSON.stringify(
                {
                    date: (new Date()).toUTCString()
                }
            )
        )
        console.log('---------------\n\n')
    }

    if(req.url === '/measures/temperature' && req.method === 'POST'){

        console.log(
            JSON.parse(
                req.payload.toString()
            )
        )

        res.end(
            JSON.stringify(
                {
                    response: true
                }
            )
        )
        console.log('---------------\n\n')
    }

})
 
coap_server.listen(port,() => {
    console.log(`Coap server listening on port ${port}`)
})