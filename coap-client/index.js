const coap = require("coap")
const bl = require('bl')

const coapServer = 'localhost'

const sendDateRequest = () => {
    console.log('-------------------')
    console.log('GET ==> Date request! \n')

    const coapConnection = {
        host: coapServer,
        pathname: '/date',
        method: 'GET',
        confirmable: true,
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const req = coap.request(coapConnection)

    req.on('response', (res) => {
    
        console.log('Response code:', res.code)

        if (res.code !== '2.05') {
            return process.exit(1)
        }
        
        res.pipe(
            bl(
                (err, data) => {
                    const json = JSON.parse(data)
                    console.log(json.date)
                    console.log('------------------- \n\n')
                }
            )
        )
        
        
        res.on('end', () => {
            setTimeout(() => {
                sendDateRequest()
            }, 5000)
        })
    
    })
    
    req.end()
}

const sendMeasures = () => {
    console.log('-------------------')
    console.log('POST ==> New Measure! \n')

    const getRandomIntInclusive = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    const coapConnection = {
        host: coapServer,
        pathname: '/measures/temperature',
        method: 'POST',
        confirmable: true,
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const req = coap.request(coapConnection)

    const payload = {
        date: (new Date()).toUTCString(),
        type: 'number',
        units: 'ºC',
        measure: getRandomIntInclusive(-20,50),
        name: 'temperature'
    }
  
    req.write(JSON.stringify(payload));

   
    req.on('response', (res) => {
    
        console.log('Response code:', res.code)

        if (res.code !== '2.05') {
            return process.exit(1)
        }
        
        res.pipe(
            bl(
                (err, data) => {
                    const json = JSON.parse(data)
                    console.log(json)
                    console.log('------------------- \n\n')
                }
            )
        )
        
        res.on('end', () => {
            setTimeout(() => {
                sendMeasures()
            }, 10000)
        })
    
    })
    
    req.end()
}

sendDateRequest()

setTimeout(() => { 
    sendMeasures()
}, 1000)
