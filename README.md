## Probando el protocolo CoAP con Node.js

Para probar un poco el uso del protocolo CoAP, vamos a montar el siguiente escenario, usando el paquete [`coap`](https://github.com/mcollina/node-coap#readme) de Node.js. 

![Ejemplo CoAP en Node.js](https://www.miguelsbm.com/png/contenido/el-protocolo-coap/7.png)

Igual podemos hacerlo con otros lenguajes y su correspondiente librería, ya que existen numerosas implementaciones de éste. Podemos verlas [aquí](https://coap.technology/impls.html).

### Servidor

Paso a paso:
* Creamos el servidor CoAP especificando el tipo de contenido de las comunicaciones y en caso de no ser así, rechazandose.
* Especificamos el funcionamiento del evento `request` en caso de recibir una petición de un cliente.
	* Metodo `GET` a la dirección `/date` retorna la fecha actual del dispositivo servidor.
	* `POST` al la dirección `/measures/temperature` obtiene la temperatura del equipo cliente pudiendo hacer con ésta información lo que quiera, por ejemplo almacenarla en una base de datos. Responde al cliente indicando que todo ha ido bien, o podríamos acabar ahí.
* Inicia la escucha del protocolo CoAP en el puerto especificado.

### Cliente

Paso a paso:
* Se especifican y ejecutan con un segundo de diferencia dos opercaciónes cliente para el servidor CoAP que se configure:
	* Petición de la fecha actual en el servidor, que se repite cada 5 segundos.
	* Envío de un ejemplo de medida de temperatura al Servidor, que se repite cada 10 segundos.

### Resultado

Si todo ha ido bien, deberíamos obtener el siguiente resultado:

![Ejemplo CoAP en Node.js](https://www.miguelsbm.com/png/contenido/el-protocolo-coap/test-result.png)

Básicamente consiste en:
* Petición de la fecha exacta en el servidor por parte del cliente y recepción de ésta cada 5 segundos.
* Envío al servidor de un ejemplo de medida de temperatura en el cliente cada 10 segundos.

Este paquete nos permite hacer muchísimo más, es cuestión de estudiar sus capacidades y explotarlas.

También podemos hacer uso del paquete [`coap-cli`](https://github.com/avency/coap-cli), que nos proporciona una interfaz por línea de comandos para el protocolo CoAP.
